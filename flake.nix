{
  description = "Just fetch stuff";

  outputs = { self, nixpkgs }: let
    forAllSystems = nixpkgs.lib.genAttrs [ "x86_64-linux" "x86_64-darwin" "i686-linux" "aarch64-linux" ];
  in {
    devShell = forAllSystems (system: let
      pkgs = nixpkgs.legacyPackages."${system}";
    in pkgs.mkShell {
      # Things to be put in $PATH
      nativeBuildInputs = with pkgs; [ pkgconfig ];

      # Libraries to be installed
      buildInputs = with pkgs; [ openssl ];
    });
  };
}
