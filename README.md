# just-fetch ![Crates.io](https://img.shields.io/crates/v/just-fetch)

The answer when your prayer "Please just fetch the file, I don't care
if it's local or if it's pointing to a gzipped URL or whatever."

Used by [scaff](https://gitlab.com/jD91mZM2/scaff).
